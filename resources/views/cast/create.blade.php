@extends('adminlte.partials.master')

@section('createcast')
<div class="card m-4">
    <div class="card-header">
        Featured
    </div>
    <div class="card-body">
        <h5 class="card-title">Special title treatment</h5>
        <br>
        <form action="{{ url('cast/create') }}" method="post">
            @csrf
            <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" name="name">
            </div>
            @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div class="form-group">
                <label>Umur</label>
                <input type="number" class="form-control" name="umur">
            </div>
            @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div class="form-group">
                <label>Bio</label>
                <br>
                <textarea name="bio" cols="50" rows="5" placeholder="deskripsikan sedikit tentang cast anda"></textarea>
            </div>
            @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <div class="tombolsubmit">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
</div>
@endsection