@extends('adminlte.partials.master')

@section('datacast')
<div class="card mb-4">
    <div class="card-header">
        Data Cast
    </div>
    <div class="tombol-tambah">
        <a href="{{ 'cast/create' }}" class="btn btn-primary ml-3 mt-3" type="submit">Tambah Data</a>
    </div>
    <div class="table-responsive m-3">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th>id</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Bio</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($cast as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->umur }}</td>
                    <td>{{ $item->bio }}</td>
                    <td>
                        <form action="/cast/{{ $item->id }}" method="post">
                            @csrf
                            @method('delete')
                            <a href="/cast/{{ $item->id }}" class="btn btn-info btn-sm">Detail data</a>
                            <a href="/cast/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" class="btn btn-danger btn-sm" value="DELETE">
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

