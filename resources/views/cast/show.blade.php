@extends('adminlte.partials.master')

@section('showcastid')
<div class="card m-4">
    <div class="card-header">
        Data Detail {{ $cast->name }}
    </div>
    <div class="datacast m-4">
    <h4>Nama : {{ $cast->name }}</h4>
    <h4>Umur : {{ $cast->umur }}</h4>
    <h4>Bio : {{ $cast->bio }}</h4>
</div>

<div class="tombol-edit m-3">
<a href="{{ url('cast') }}" class="btn btn-secondary btn-small">Kembali</a>
<a href="/cast/{{ $cast->id }}/edit" class="btn btn-warning btn-small">Edit</a>
</div>
</div>
@endsection